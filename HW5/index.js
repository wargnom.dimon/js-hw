//Добавление \ перед специальными символами. Для использования специальных символов как обычных.

"use strict";

function createNewUser() {
  let name = prompt("Введите имя", "Александр");
  while (!name.match(/^[a-zA-Zа-яА-Я]+$/)) {
    name = prompt("Неверный формат имени", `${name}`);
  }

  let surename = prompt("Введите фамилию", "Пушкин");
  while (!surename.match(/^[a-zA-Zа-яА-Я]+$/)) {
    surename = prompt("Неверный формат фамилии  ", `${surename}`);
  }

  let birthdaydate = prompt(
    "Введите дату рождения в формате dd.mm.yyyy ",
    "06.06.1799"
  );
  while (!birthdaydate.match(/^\d{2}.\d{2}.\d{4}$/)) {
    birthdaydate = prompt(
      "Неверный формат, введите дату рождения в формате dd.mm.yyyy  ",
      `${birthdaydate}`
    );
  }

  return {
    firstName: name,
    lastName: surename,
    birthday: birthdaydate,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      return (
        ((new Date().getTime() -
          new Date(
            this.birthday.slice(-4) +
              this.birthday.slice(2, 5) +
              "." +
              this.birthday.slice(0, 2)
          )) /
          (24 * 3600 * 365.25 * 1000)) |
        0
      );
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
}

let newUser = createNewUser();

//console.log(createNewUser());
console.log(newUser.getAge() + " years");
console.log(newUser.getLogin());
console.log(newUser.getPassword());
