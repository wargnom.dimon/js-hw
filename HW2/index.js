//Для выполнения однотипного кода несколько раз.

"use strict";

let num = +prompt("Введите целое число", "");

while (num % 1 !== 0) {
  num = +prompt("Введите целое число", "");
}
if (num < 5) {
  console.log("Sorry, no numbers");
} else
  for (let i = 1; i <= num; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
