"use strict";
let btnCode = document.querySelectorAll(".btn");

document.addEventListener("keydown", function (event) {
  return btnCode.forEach((item) => {
    let btnText = item.innerHTML;
    btnText.toLowerCase() === event.key.toLowerCase()
      ? item.classList.toggle("btn-blue")
      : item.classList.remove("btn-blue");
  });
});
