let divTabs = function (target) {
  let _elemTabs =
      typeof target === "string" ? document.querySelector(target) : target,
    titleTab = function (tabContent) {
      let dataTabTitle, tabContentActive, dataTabTitleActive;
      dataTabTitle = document.querySelector(tabContent.getAttribute("id"));
      tabContentActive = tabContent.parentElement.querySelector(
        ".service-content-box.active"
      );
      dataTabTitleActive = dataTabTitle.parentElement.querySelector(
        ".service-content-textbox.active"
      );

      if (tabContent === tabContentActive) {
        return;
      }
      tabContentActive.classList.toggle("active");
      dataTabTitleActive.classList.toggle("active");
      tabContent.classList.toggle("active");
      dataTabTitle.classList.toggle("active");
    };

  _elemTabs.addEventListener("click", function (e) {
    let tabContent = e.target;
    e.preventDefault();
    titleTab(tabContent);
  });
};

divTabs(".service-content");

/*************************************************** */
let workContentImages = document.querySelector(".work-content-images");
let clearArr = ["graphic-design", "web-design", "landing-page", "wordpress"];
let i = 1;
let filterTabs = document.querySelectorAll(".work-content-box");
let filterTabsActive = document.querySelector(".work-content-box.active");
function createList(clearArr) {
  let newArr = clearArr
    .map((item) => {
      if (Array.isArray(item)) {
        return `${createList(item)}`;
      }
      return `<div class="work-all" name="${item}"><img class="img-content-box"
      
      src=./img/${item}/${item}${i}.jpg><div class="img-hover-box">
      <svg width="41" height="40" viewBox="0 0 44 43" fill="none"  xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z" fill="#18CFAB"/></svg>
      <svg width="41" height="40" viewBox="0 0 44 43" fill="none" stroke="#18CFAB" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M15,17 l12,0 l-0,11 l-11,0 l0,-11" fill="#18CFAB"/></svg>
      <p>CREATIVE DESIGN</p> <span>${("-" + item)
        .split("-")
        .map((word, index) =>
          index == 0 ? word : word[0].toUpperCase() + word.slice(1)
        )
        .join(" ")}</span></div>
      </div>`;
    }, i++)
    .join("");
  return `${newArr}`;
}

while (workContentImages.childNodes.length !== 12) {
  workContentImages.insertAdjacentHTML("beforeend", createList(clearArr));
}

let btnWork = document.querySelector(".btns.work.btn-load-btn");

btnWork.addEventListener("click", function () {
  let btnWorkStatus = document.querySelector(".btn-status.work");
  btnWorkStatus.classList.add("active");
  function loading() {
    if (workContentImages.childNodes.length === 12)
      while (workContentImages.childNodes.length !== 24) {
        btnWorkStatus.classList.remove("active");
        workContentImages.insertAdjacentHTML("beforeend", createList(clearArr));
      }
    else if (workContentImages.childNodes.length === 24)
      while (workContentImages.childNodes.length !== 36) {
        btnWorkStatus.classList.remove("active");
        workContentImages.insertAdjacentHTML("beforeend", createList(clearArr));
        btnWork.style.display = "none";
      }
    filterTabs.forEach((item) => {
      filterTabsActive = document.querySelector(".work-content-box.active");
      let filtredBox = document.querySelectorAll(".work-all");
      item.classList.toggle("active");
      filterTabsActive.classList.toggle("active");
      for (let i = 0; i < filtredBox.length; i++) {
        if (item.getAttribute("id") !== filtredBox[i].getAttribute("name")) {
          filtredBox[i].classList.add("hide");
        }
        if (item.getAttribute("id") === filtredBox[i].getAttribute("name")) {
          filtredBox[i].classList.remove("hide");
        }
        if (item.getAttribute("id") === "work-all") {
          filtredBox[i].classList.remove("hide");
        }
      }
    });
    clearTimeout(loadTimer);
  }
  let loadTimer = setTimeout(loading, 3000);
});
filterTabs.forEach((item) => {
  item.addEventListener("click", function () {
    filterTabsActive = document.querySelector(".work-content-box.active");
    let filtredBox = document.querySelectorAll(".work-all");
    item.classList.toggle("active");
    filterTabsActive.classList.toggle("active");
    for (let i = 0; i < filtredBox.length; i++) {
      if (item.getAttribute("id") !== filtredBox[i].getAttribute("name")) {
        filtredBox[i].classList.add("hide");
      }
      if (item.getAttribute("id") === filtredBox[i].getAttribute("name")) {
        filtredBox[i].classList.remove("hide");
      }
      if (item.getAttribute("id") === "work-all") {
        filtredBox[i].classList.remove("hide");
      }
    }
  });
});
/************************************************ */
$(".slider-for").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".slider-nav",
});
$(".slider-nav").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: ".slider-for",
  dots: false,
  centerMode: false,
  focusOnSelect: true,
});
$(".gallery-content").masonry({
  itemSelector: ".gallery-content-item",
  columnWidth: 386,
});
/********************************************* */
let btnGallery = document.querySelector(".btns.gallery.btn-load-btn");
let galleryImages = document.querySelector(".gallery-content");
let num = 19;
$(".btns.gallery.btn-load-btn").on("click", function () {
  let btnGalleryStatus = document.querySelector(".btn-status.gallery");
  btnGalleryStatus.classList.add("active");

  function addItems() {
    {
      num += 1;
      // create new item elements

      var $items = $(
        '<img class="gallery-content-item" src="./img/gallery/' +
          `${num}` +
          '.jpg"' +
          "alt=" +
          `${num}` +
          "/>"
      );

      // append items to grid
      $(".gallery-content")
        .append($items)
        // add and lay out newly appended items
        .masonry("appended", $items)
        .masonry();
    }
    btnGalleryStatus.classList.remove("active");
    clearTimeout(loadGalleryTimer);
  }
  let loadGalleryTimer = setTimeout(addItems, 3000);
});
