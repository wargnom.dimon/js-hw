//Для повторения одного и того же действия(вызвов функции) в разных частях програмы без повторения кода (обьявления функции)
//Для передачи значений параметров функции. Можно задать значение по умолчанию.

"use strict";

function calc() {
  let operation = prompt("Выберите операцию: +, -, *, /", "");

  while (
    operation !== "+" &&
    operation !== "-" &&
    operation !== "*" &&
    operation !== "/"
  )
    operation = prompt(
      "Вы выбрали неверную операцию, Выберите  операцию: +, -, *, /",
      `${operation}`
    );

  let num1 = +prompt("Введите число a", "");
  let num2 = +prompt("Введите число b", "");

  let result = 0;

  while (isNaN(num1 || num2)) {
    num1 = +prompt(
      "Ошибка, одно из введеных значений не есть число, Введите число a",
      `${num1}`
    );
    num2 = +prompt(
      "Ошибка, одно из введеных значений не есть число, Введите число b",
      `${num2}`
    );
  }

  if (operation == "+") {
    result = num1 + num2;
  }
  if (operation == "-") {
    result = num1 - num2;
  }
  if (operation == "*") {
    result = num1 * num2;
  }
  if (operation == "/") {
    while (num2 === 0) {
      num2 = +prompt(
        "На ноль делить нельзя, Введите число b не равное нулю",
        `${num2}`
      );
    }
  }
  if (operation == "/") {
    result = num1 / num2;
  }
  return result;
}
console.log(calc());
