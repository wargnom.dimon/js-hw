//Для повторения одного и того же действия(вызвов функции) в разных частях програмы без повторения кода (обьявления функции)
//Для передачи значений параметров функции. Можно задать значение по умолчанию.

"use strict";

function calc(num1, operation, num2) {
  switch (operation) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      while (num2 === 0)
        num2 = +prompt(
          "На ноль делить нельзя, Введите число b не равное нулю",
          `${num2}`
        );
      return num1 / num2;
  }
}

let operation = prompt("Выберите операцию: +, -, *, /", " ");

while (
  operation !== "+" &&
  operation !== "-" &&
  operation !== "*" &&
  operation !== "/"
)
  operation = prompt(
    "Вы выбрали неверную операцию, Выберите  операцию: +, -, *, /",
    `${operation}`
  );

let num1 = +prompt("Введите число a", " ");
while (isNaN(num1)) {
  num1 = +prompt(
    "Ошибка, введеное значение не есть число, Введите число a",
    `${num1}`
  );
}
let num2 = +prompt("Введите число b", " ");
while (isNaN(num2)) {
  num2 = +prompt(
    "Ошибка, введеное значение не есть число, Введите число b",
    `${num2}`
  );
}
console.log(calc(num1, operation, num2));
