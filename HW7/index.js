let clearArr = [
  "hello",
  "world",
  "Kiev",
  ["Borispol", ["svyat", "nivki"], "Irpin", ["Goloseevskiy", "Pecherskiy"]],
  "Kharkiv",
  "Odessa",
  "Lviv",
  34,
  45,
];

let div = document.querySelector(".list");

function createList(clearArr) {
  let newArr = clearArr
    .map((item) => {
      if (Array.isArray(item)) {
        return `${createList(item)}`;
      }
      return `<li>${item}</li>`;
    })
    .join("");
  return `<ul>${newArr}</ul>`;
}

let btnstart = document.querySelector(".btn-start");
btnstart.onclick = function () {
  div.insertAdjacentHTML("beforeend", createList(clearArr));
  btnstart.style.opacity = 0;
};

let timer = document.querySelector(".timer");

setInterval(() => {
  if (+timer.innerHTML > 0) {
    timer.innerHTML -= 1;
  }
  if (+timer.innerHTML === 0) {
    clearInterval(timer);
    document.body.innerHTML = "";
  }
}, 1000);

let btn = document.querySelector(".btn");
btn.onclick = function () {
  timer.innerHTML = 1;
  let text = document.querySelector(".text");
  text.innerHTML = "до очистки осталось";
  btnstart.style.opacity = 0;
};
