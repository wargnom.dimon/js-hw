"use strict";

let images = document.querySelectorAll(".image-to-show");
let currImg = 0;
let imgTimer = setInterval(nextImg, 3000);

function nextImg() {
  images[currImg].classList.toggle("active");
  currImg = (currImg + 1) % images.length;
  images[currImg].classList.toggle("active");
}

let btnP = document.querySelector(".btn-pause");
let btnR = document.querySelector(".btn-resume");

btnP.addEventListener("click", function () {
  clearInterval(imgTimer);
});
btnR.addEventListener("click", function () {
  clearInterval(imgTimer);
  imgTimer = setInterval(nextImg, 3000);
});
