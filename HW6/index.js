"use strict";

//let filterBy = (arr, type) => arr.filter((item) => typeof item !== type);

//console.log(filterBy(["hello", "world", 23, "23", null], "string"));

let arr = ["hello", "world", 23, "23", null];

let type = "string";

function filterBy(arr, type) {
  let result = [];
  for (let str of arr) {
    if (typeof str !== type) {
      result.push(str);
    }
  }
  return result;
}

let newArr = filterBy(arr, type);

console.log(newArr);
