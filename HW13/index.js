"use strict";

if (localStorage.getItem("theme") != "") {
  loadcssfile(localStorage.getItem("theme"));
}

function loadcssfile(filename) {
  if (filename != "") {
    localStorage.setItem("theme", filename);
  }

  let fileref = document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", filename);
  fileref.setAttribute("id", "theme_css");

  if (typeof fileref !== "undefined")
    document.querySelector("#theme_css").replaceWith(fileref, "");
}

let btns = document.querySelectorAll(".btn-lang");
let ruLangBtn = document.querySelector(".ru-lang");
let enLangBtn = document.querySelector(".en-lang");
let uaLangBtn = document.querySelector(".ua-lang");
for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    if (btns[i] === ruLangBtn) {
      loadcssfile("./css/ru-style.css");
    }
    if (btns[i] === enLangBtn) {
      loadcssfile("./css/en-style.css");
    }
    if (btns[i] === uaLangBtn) {
      loadcssfile("./css/ua-style.css");
    }
  });
}
