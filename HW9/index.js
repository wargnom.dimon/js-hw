"use strict";
let divTabs = function (target) {
  let _elemTabs =
      typeof target === "string" ? document.querySelector(target) : target,
    titleTab = function (tabContent) {
      let dataTabTitle, tabContentActive, dataTabTitleActive;
      dataTabTitle = document.querySelector(tabContent.getAttribute("href"));
      tabContentActive = tabContent.parentElement.querySelector(
        ".tab-content.active"
      );
      dataTabTitleActive = dataTabTitle.parentElement.querySelector(
        ".data-tab-title.active"
      );

      if (tabContent === tabContentActive) {
        return;
      }
      tabContentActive.classList.toggle("active");
      dataTabTitleActive.classList.toggle("active");
      tabContent.classList.toggle("active");
      dataTabTitle.classList.toggle("active");
    };

  _elemTabs.addEventListener("click", function (e) {
    let tabContent = e.target;
    e.preventDefault();
    titleTab(tabContent);
  });
};

divTabs(".centered-content");
